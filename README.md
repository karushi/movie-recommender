**Welcome to Movie Recommender**

This web application is a movie recommender system.


---

## How to install/run

You will need the following tools if you want to run it locally.

1. Install python and make sure pip is installed as well.
2. Install all packages in ```requirements.txt```.