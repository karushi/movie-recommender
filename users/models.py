from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    date_of_birth = models.DateField()
    
    REQUIRED_FIELDS = [
        'email',
        'date_of_birth',
    ]
    
    def __str__(self):
        return self.first_name+' '+self.last_name

class Movie(models.Model):
    title = models.CharField(max_length=100)
    director = models.CharField(max_length=100)
    year = models.IntegerField()
    description = models.TextField(max_length=300)
    genre = models.CharField(max_length=20)

    def __str__(self):
        return self.title+' by '+self.director