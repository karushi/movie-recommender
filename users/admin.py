from django.contrib import admin
from users.models import User, Movie

admin.site.register(User)
admin.site.register(Movie)