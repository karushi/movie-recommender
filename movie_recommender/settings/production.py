from .base import *

DEBUG = False

ALLOWED_HOSTS = [
    'hosted_address.com',
]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backend.memcached.MemCachedCache',
        'LOCATION': '127.0.0.1:11211',
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_TLS = True