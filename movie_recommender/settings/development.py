from .base import *

DEBUG = True

INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]
STATIC_URL = '/static/'
STATIC_ROOT = join(BASE_DIR, '../static/')
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': '',
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'USER': data['DATABASE_USER'],
        'NAME': data['DATABASE_NAME'],
        'PASSWORD': data['DATABASE_PASSWORD'],
        'HOST': data['DATABASE_HOST'],
        'PORT': data['DATABASE_PORT'],
        'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
    },
}
